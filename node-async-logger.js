module.exports = (function(){

  // File system
  const fs = require('fs');
  // Date format
  const df = require('dateformat');
  // String format
  const format = require('string-format');
  // System sleep
  const sleep = require('system-sleep');

  const Cls = function(){
    this._fd = false;
    this._pending = false;
    this._err = false;
    this._dateformat = 'yyyy-mm-dd HH:MM:ss.l';
    this._logformat = '{datetime} {level} {message}\n';
    this._cache = [];
    this._timestamps = [];
  };

  Cls.prototype.dateformat = function(){
    if(arguments.length <= 0){
      return this._dateformat;
    }else{
      this._dateformat = arguments[0];
      return this;
    }
  };

  Cls.prototype.logformat = function(){
    if(arguments.length <= 0){
      return this._logformat;
    }else{
      this._logformat = arguments[0];
      return this;
    }
  };

  Cls.prototype.open = function(path){
    this._err = false;
    this._pending = true;

    const me = this;
    fs.open(path, 'a', (err, fd)=>{
      if(err){
        /* */ console.error(err);
        me._err = true;
        me._pending = false;
      }else{
        me._pending = false;
        me._fd = fd;
      }

      me._ready();
    });

    return this;
  };

  Cls.prototype.flush = function(){
    while(this._cache.length > 0){
      this._ready();
      sleep(100);
    };
    return this;
  };

  Cls.prototype.close = function(){
    this.flush();

    if(this._fd){
      let endflg = false;
      fs.close(this._fd, (err)=>{
        if(err){
          /* */ console.error(err);
        }
        endflg = true;
      });
      while(! endflg) sleep(100);

      this._fd = false;
      this._err = false;
    }

    return this;
  };

  Cls.prototype._ready = function(){
    if(this._pending) return this;

    if(this._cache.length <= 0) return this;

    let msg = this._cache.splice(0, 1)[0];
    return this.log(msg.level, msg.message, msg.time);
  };

  Cls.prototype.log = function(level, message, time){
    if(this._pending){
      this._cache.push({
        level  : level,
        message: message,
        time   : time ? time : new Date()
      });
      return this;
    }

    this._pending = true;
    
    let text = format(this._logformat, {
      datetime: df(time ? time : new Date(), this._dateformat),
      level   : level,
      message : message
    });

    if(! this._fd || this._err){
      process.stdout.write(text);
      this._pending = false;
      this._ready();
    }else{
      const me = this;
      fs.write(this._fd, text, (err)=>{
        if(err){
          /* */ console.error(err);
          /* */ console.log(text);
          me._err = true;
        }
        me._pending = false;
        me._ready();
      });
    }

    return this;
  };

  Cls.prototype.error = Cls.prototype.err = Cls.prototype.e = function(message){
    return this.log('ERROR', message);
  };

  Cls.prototype.warn = Cls.prototype.warning = Cls.prototype.w = function(message){
    return this.log('WARN', message);
  };

  Cls.prototype.info = Cls.prototype.information = Cls.prototype.i = function(message){
    return this.log('INFO', message);
  };

  Cls.prototype.debug = Cls.prototype.d = function(message){
    return this.log('DEBUG', message);
  };

  Cls.prototype.time = function(name, nolog){
    let time = new Date();
    this._timestamps[name] = time;
    if(! nolog) this.log('TIME', `${name} (${time})`);
    return time;
  };

  Cls.prototype.eslapse = function(name, reset, nolog){
    if(! this._timestamps[name]){
      this.time(name);
      return undefined;
    }

    let otime = this._timestamps[name];
    let time = new Date();
    let eslapse = time - otime;
    if(! nolog) this.log('ESLAPSE', `${name} (${eslapse} ms)`);

    if(reset){
      this._timestamp[name] = time;
      if(! nolog) this.log('TIME', `${name} (${time})`);
    }

    return eslapse;
  };

  const Func = function(){
    if(arguments.length <= 0){
      return new Cls();
    }else{
      let ins = new Cls();
      return ins.open(arguments[0]);
    }
  };

  return Func;
})();
