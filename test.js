const logger = require('./node-async-logger.js');
let log = logger();

log
.information('This is information')
.info('This is information')
.i('This is information')
.warning('This is warning')
.warn('This is warning')
.w('This is warning')
.error('This is error')
.err('This is error')
.e('This is error')
.debug('This is debug')
.d('This is debug')

.open('log.txt')
.information('This is information')
.info('This is information')
.i('This is information')
.warning('This is warning')
.warn('This is warning')
.w('This is warning')
.error('This is error')
.err('This is error')
.e('This is error')
.debug('This is debug')
.d('This is debug')
.close()

.information('This is information')
.info('This is information')
.i('This is information')
.warning('This is warning')
.warn('This is warning')
.w('This is warning')
.error('This is error')
.err('This is error')
.e('This is error')
.debug('This is debug')
.d('This is debug')
;

console.log(log.time('time 1'));
console.log(log.eslapse('time 1'));
console.log(log.time('time 2', true));
console.log(log.eslapse('time 2', false, true));
