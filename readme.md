# node-async-logger
Asynchronous logger on NodeJs.

## Install
```bash
npm install node-async-logger
```

## Usage

```javascript
const logger = require('node-async-logger');

let log = logger('path/to/log/file');

log.error('This is error');

log.close(); // ! Important
```

## Methods

### error, err, e
Log error information

##### Example
```javascript
log.error('This is error');
// yyyy-mm-dd hh24:mi:ss.ms ERROR This is error
```

### warning, warn, w
Log warning information

##### Example
```javascript
log.warn('This is warning');
// yyyy-mm-dd hh24:mi:ss.ms WARN This is warning
```

### information, info, i
Log information

##### Example
```javascript
log.info('This is information');
// yyyy-mm-dd hh24:mi:ss.ms INFO This is information
```

### debug, d
Log debugging information

##### Example
```javascript
log.debug('This is debugging');
// yyyy-mm-dd hh24:mi:ss.ms DEBUG This is debugging
```

### time(*name*, *nolog*)
Mark current time as the starting point for calculating eslapse time

##### Parameters
* **name** String
* **nolog** Boolean (optional, default: *false*)
* **return** Date object

##### Example
```javascript
log.time('somename');
// yyyy-mm-dd hh24:mi:ss.ms TIME somename (Thu Sep 28 2017 15:32:27 GMT+0000 (UTC))
```

### eslapse(*name*, *reset*, *nolog*)
Get eslapsed time from the marked time named by *name*

##### Parameters
* **name** String
* **reset** Boolean (optional, default: *false*)
* **nolog** Boolean (optional, default: *false*)
* **return** Number Eslapsed time (ms)

##### Examples
```javascript
log.eslapse('somename');
// yyyy-mm-dd hh24:mi:ss.ms ESLAPSE somename ( 000 ms )
```

## Bugs or feature requests
Please contact to [email](mailto:lchild358@yahoo.com.vn)

(Please add the prefix "[node-async-logger]" into the email title. Thanks!)

## License
[ISC](https://opensource.org/licenses/ISC)
